package com.unisabana.edu;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Restaurante {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	Long id;
	@Persistent
	int noVentasOnline;
	@Persistent
	int noVentasFisicas;
	@Persistent
	String nombre;
	@Persistent
	int nivelCongestion;
	
	public Restaurante()
	{
		
	}
	
	public Restaurante(Long id)
	{
		super();
		this.id=id;
	}
	
	public Restaurante(Long id, int noVentasOnline, int noVentasFisicas, String nombre, int nivelCongestion)
	{
		super();
		this.id=id;
		this.noVentasOnline= noVentasOnline;
		this.noVentasFisicas = noVentasFisicas;
		this.nombre=nombre;
		this.nivelCongestion = nivelCongestion;
		
	}

	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result +((id==null)? 0:id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(this==obj)
			return true;
		if(obj==null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Restaurante other = (Restaurante) obj;
		if (id == null) {
			if (other.id != null)
				return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNoVentasOnline() {
		return noVentasOnline;
	}

	public void setNoVentasOnline(int noVentasOnline) {
		this.noVentasOnline = noVentasOnline;
	}

	public int getNoVentasFisicas() {
		return noVentasFisicas;
	}

	public void setNoVentasFisicas(int noVentasFisicas) {
		this.noVentasFisicas = noVentasFisicas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNivelCongestion() {
		return nivelCongestion;
	}

	public void setNivelCongestion(int nivelCongestion) {
		this.nivelCongestion = nivelCongestion;
	}

	@Override
	public String toString(){
		return "Quote[id="+ id + ", noVentasOnline = " + noVentasOnline + ", noVentasFisicas=" + noVentasFisicas + ", nombre=" + nombre + ", nivelCongestion=" + nivelCongestion +"]";
	}
}

