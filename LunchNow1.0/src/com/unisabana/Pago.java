package com.unisabana;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import java.util.ArrayList;
@PersistenceCapable(identityType = IdentityType.APPLICATION)

public class Pago {
    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long idPago;
    @Persistent
    private Long idUsuario;
    @Persistent
    private Long idPuntoDeComida;
    @Persistent
    private String date;
    @Persistent
    private float total;
    @Persistent
    private boolean estadoPago;
    @Persistent
    private ArrayList<Long> listaProductos;
    @Persistent
    private ArrayList<Integer> cantidadSolicitada;
    @Persistent
    private String hora;
    public Pago() {
    }
    public Pago(Long idPago) {
    	super();
    	this.idPago = idPago;
    }

	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result +((idPago==null)? 0:idPago.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Pago other = (Pago) obj;
	if (idPago == null) {
		if (other.idPago != null)
			return false;
		} else if (!idPago.equals(other.idPago))
			return false;
		return true;
	}
	public Pago(Long idPago, Long idUsuario, Long idPuntoDeComida, String date,
			float total, boolean estadoPago, ArrayList<Long> listaProductos,
			ArrayList<Integer> cantidadSolicitada, String hora) {
		super();
		this.idPago = idPago;
		this.idUsuario = idUsuario;
		this.idPuntoDeComida = idPuntoDeComida;
		this.date = date;
		this.total = total;
		this.estadoPago = estadoPago;
		this.listaProductos = listaProductos;
		this.cantidadSolicitada = cantidadSolicitada;
		this.hora = hora;
	}
	public Long getIdPago() {
		return idPago;
	}
	public void setIdPago(Long idPago) {
		this.idPago = idPago;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Long getIdPuntoDeComida() {
		return idPuntoDeComida;
	}
	public void setIdPuntoDeComida(Long idPuntoDeComida) {
		this.idPuntoDeComida = idPuntoDeComida;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public boolean isEstadoPago() {
		return estadoPago;
	}
	public void setEstadoPago(boolean estadoPago) {
		this.estadoPago = estadoPago;
	}
	public ArrayList<Long> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(ArrayList<Long> listaProductos) {
		this.listaProductos = listaProductos;
	}
	public ArrayList<Integer> getCantidadSolicitada() {
		return cantidadSolicitada;
	}
	public void setCantidadSolicitada(ArrayList<Integer> cantidadSolicitada) {
		this.cantidadSolicitada = cantidadSolicitada;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	@Override
	public String toString() {
		return "Pago [idPago=" + idPago + ", idUsuario=" + idUsuario
				+ ", idPuntoDeComida=" + idPuntoDeComida + ", date=" + date
				+ ", total=" + total + ", estadoPago=" + estadoPago
				+ ", listaProductos=" + listaProductos
				+ ", cantidadSolicitada=" + cantidadSolicitada + ", hora="
				+ hora + "]";
	}
		

}
