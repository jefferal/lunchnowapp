package com.unisabana;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)

public class ComentariosYsugerencias {
    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long id;
    @Persistent
	private String texto;
    public ComentariosYsugerencias() {
    }
    public ComentariosYsugerencias(Long id) {
    	super();
    	this.id= id;
    }
    public ComentariosYsugerencias(Long id, String texto) {
		super();
		this.id = id;
		this.texto = texto;
	}
    @Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result +((id==null)? 0:id.hashCode());
		return result;
	}
    @Override
    public boolean equals(Object obj) {
    if (this == obj)
    	return true;
    if (obj == null)
    	return false;
    if (getClass() != obj.getClass())
    	return false;
    ComentariosYsugerencias other = (ComentariosYsugerencias) obj;
    if (id == null) {
    	if (other.id != null)
    		return false;
    	} else if (!id.equals(other.id))
    		return false;
    	return true;
    }

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	@Override
	public String toString() {
		return "comentariosYsugerencias [id=" + id + ", texto=" + texto + "]";
	}
	
}
