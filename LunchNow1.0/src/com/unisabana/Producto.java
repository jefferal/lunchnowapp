package com.unisabana;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Producto{
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	Long id;
	@Persistent
	String nombre;
	@Persistent
	Integer Precio;
	@Persistent
	Integer unidadesVendidas;
	
	public Producto()
	{
		
	}
	public Producto(Long id)
	{
		super();
		this.id=id;
	}
	
	public Producto(Long id, String nombre, Integer precio,
			Integer unidadesVendidas) {
		super();
		this.id = id;
		this.nombre = nombre;
		Precio = precio;
		this.unidadesVendidas = unidadesVendidas;
	}
	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result +((id==null)? 0:id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Producto other = (Producto) obj;
	if (id == null) {
		if (other.id != null)
			return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getPrecio() {
		return Precio;
	}
	public void setPrecio(Integer precio) {
		Precio = precio;
	}
	public Integer getUnidadesVendidas() {
		return unidadesVendidas;
	}
	public void setUnidadesVendidas(Integer unidadesVendidas) {
		this.unidadesVendidas = unidadesVendidas;
	}
	@Override
	public String toString() {
		return "Producto [id=" + id + ", nombre=" + nombre + ", Precio="
				+ Precio + ", unidadesVendidas=" + unidadesVendidas + "]";
	}
	
	
}