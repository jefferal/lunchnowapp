package com.unisabana;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
@PersistenceCapable(identityType = IdentityType.APPLICATION)
//Recuerden usar el sueldo y el sueldoAviso solo para los estudiantes
public class Usuario {
    @PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long id;
    @Persistent
    private String sueldo;
    @Persistent
    private String sueldoAviso;
    @Persistent
    private String nombre;
    @Persistent
    private String correo;
    @Persistent
    private String clave;
    @Persistent
    private String tipo;
	
    public Usuario() {
    }
    public Usuario(Long idUsuario) {
    	super();
    	this.id = idUsuario;
    }
	
	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result +((id==null)? 0:id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Usuario other = (Usuario) obj;
	if (id == null) {
		if (other.id != null)
			return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public Usuario(Long id, String sueldo, String sueldoAviso, String nombre,
			String correo, String clave, String tipo) {
		super();
		this.id = id;
		this.sueldo = sueldo;
		this.sueldoAviso = sueldoAviso;
		this.nombre = nombre;
		this.correo = correo;
		this.clave = clave;
		this.tipo = tipo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSueldo() {
		return sueldo;
	}
	public void setSueldo(String sueldo) {
		this.sueldo = sueldo;
	}
	public String getSueldoAviso() {
		return sueldoAviso;
	}
	public void setSueldoAviso(String sueldoAviso) {
		this.sueldoAviso = sueldoAviso;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "Usuario [id=" + id + ", sueldo=" + sueldo + ", sueldoAviso="
				+ sueldoAviso + ", nombre=" + nombre + ", correo=" + correo
				+ ", clave=" + clave + ", tipo=" + tipo + "]";
	}

	
}
