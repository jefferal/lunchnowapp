package com.unisabana;

import com.unisabana.PMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Api(name = "comentariosysugerenciasendpoint", namespace = @ApiNamespace(ownerDomain = "unisabana.com", ownerName = "unisabana.com", packagePath = ""))
public class ComentariosYsugerenciasEndpoint {

	/**
	 * This method lists all the entities inserted in datastore.
	 * It uses HTTP GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 * persisted and a cursor to the next page.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listComentariosYsugerencias")
	public CollectionResponse<ComentariosYsugerencias> listComentariosYsugerencias(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<ComentariosYsugerencias> execute = null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(ComentariosYsugerencias.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}

			execute = (List<ComentariosYsugerencias>) query.execute();
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and accomodate
			// for lazy fetch.
			for (ComentariosYsugerencias obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<ComentariosYsugerencias> builder()
				.setItems(execute).setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET method.
	 *
	 * @param id the primary key of the java bean.
	 * @return The entity with primary key id.
	 */
	@ApiMethod(name = "getComentariosYsugerencias")
	public ComentariosYsugerencias getComentariosYsugerencias(
			@Named("id") Long id) {
		PersistenceManager mgr = getPersistenceManager();
		ComentariosYsugerencias comentariosysugerencias = null;
		try {
			comentariosysugerencias = mgr.getObjectById(
					ComentariosYsugerencias.class, id);
		} finally {
			mgr.close();
		}
		return comentariosysugerencias;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity already
	 * exists in the datastore, an exception is thrown.
	 * It uses HTTP POST method.
	 *
	 * @param comentariosysugerencias the entity to be inserted.
	 * @return The inserted entity.
	 */
	@ApiMethod(name = "insertComentariosYsugerencias")
	public ComentariosYsugerencias insertComentariosYsugerencias(
			ComentariosYsugerencias comentariosysugerencias) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (containsComentariosYsugerencias(comentariosysugerencias)) {
				throw new EntityExistsException("Object already exists");
			}
			mgr.makePersistent(comentariosysugerencias);
		} finally {
			mgr.close();
		}
		return comentariosysugerencias;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does not
	 * exist in the datastore, an exception is thrown.
	 * It uses HTTP PUT method.
	 *
	 * @param comentariosysugerencias the entity to be updated.
	 * @return The updated entity.
	 */
	@ApiMethod(name = "updateComentariosYsugerencias")
	public ComentariosYsugerencias updateComentariosYsugerencias(
			ComentariosYsugerencias comentariosysugerencias) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsComentariosYsugerencias(comentariosysugerencias)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(comentariosysugerencias);
		} finally {
			mgr.close();
		}
		return comentariosysugerencias;
	}

	/**
	 * This method removes the entity with primary key id.
	 * It uses HTTP DELETE method.
	 *
	 * @param id the primary key of the entity to be deleted.
	 */
	@ApiMethod(name = "removeComentariosYsugerencias")
	public void removeComentariosYsugerencias(@Named("id") Long id) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			ComentariosYsugerencias comentariosysugerencias = mgr
					.getObjectById(ComentariosYsugerencias.class, id);
			mgr.deletePersistent(comentariosysugerencias);
		} finally {
			mgr.close();
		}
	}

	private boolean containsComentariosYsugerencias(
			ComentariosYsugerencias comentariosysugerencias) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(ComentariosYsugerencias.class,
					comentariosysugerencias.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}

}
