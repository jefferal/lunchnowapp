package com.unisabana;

import com.unisabana.PMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Api(name = "productoendpoint", namespace = @ApiNamespace(ownerDomain = "unisabana.com", ownerName = "unisabana.com", packagePath = ""))
public class ProductoEndpoint {

	/**
	 * This method lists all the entities inserted in datastore.
	 * It uses HTTP GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 * persisted and a cursor to the next page.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listProducto")
	public CollectionResponse<Producto> listProducto(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Producto> execute = null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Producto.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}

			execute = (List<Producto>) query.execute();
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and accomodate
			// for lazy fetch.
			for (Producto obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<Producto> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET method.
	 *
	 * @param id the primary key of the java bean.
	 * @return The entity with primary key id.
	 */
	@ApiMethod(name = "getProducto")
	public Producto getProducto(@Named("id") Long id) {
		PersistenceManager mgr = getPersistenceManager();
		Producto producto = null;
		try {
			producto = mgr.getObjectById(Producto.class, id);
		} finally {
			mgr.close();
		}
		return producto;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity already
	 * exists in the datastore, an exception is thrown.
	 * It uses HTTP POST method.
	 *
	 * @param producto the entity to be inserted.
	 * @return The inserted entity.
	 */
	@ApiMethod(name = "insertProducto")
	public Producto insertProducto(Producto producto) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (containsProducto(producto)) {
				throw new EntityExistsException("Object already exists");
			}
			mgr.makePersistent(producto);
		} finally {
			mgr.close();
		}
		return producto;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does not
	 * exist in the datastore, an exception is thrown.
	 * It uses HTTP PUT method.
	 *
	 * @param producto the entity to be updated.
	 * @return The updated entity.
	 */
	@ApiMethod(name = "updateProducto")
	public Producto updateProducto(Producto producto) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsProducto(producto)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(producto);
		} finally {
			mgr.close();
		}
		return producto;
	}

	/**
	 * This method removes the entity with primary key id.
	 * It uses HTTP DELETE method.
	 *
	 * @param id the primary key of the entity to be deleted.
	 */
	@ApiMethod(name = "removeProducto")
	public void removeProducto(@Named("id") Long id) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			Producto producto = mgr.getObjectById(Producto.class, id);
			mgr.deletePersistent(producto);
		} finally {
			mgr.close();
		}
	}

	private boolean containsProducto(Producto producto) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Producto.class, producto.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}

}
