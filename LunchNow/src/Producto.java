
public class Producto {

private String nombre;
private float precio;
private int unidadesDisponibles;
private int unidadesVendidas;
public String getNombre() {
	return nombre;
}
public float getPrecio() {
	return precio;
}
public int getUnidadesDisponibles() {
	return unidadesDisponibles;
}
public int getUnidadesVendidas() {
	return unidadesVendidas;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public void setPrecio(float precio) {
	this.precio = precio;
}
public void setUnidadesDisponibles(int unidadesDisponibles) {
	this.unidadesDisponibles = unidadesDisponibles;
}
public void setUnidadesVendidas(int unidadesVendidas) {
	this.unidadesVendidas = unidadesVendidas;
}



}
